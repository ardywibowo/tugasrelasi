<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OnetomanyController;
use App\Http\Controllers\ManytomanyController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//desa
Route::get('/desa',[OnetomanyController::class, 'desa']);
Route::get('/desa/tambah',[OnetomanyController::class, 'tambahdesa']);
Route::post('/desa/simpan',[OnetomanyController::class, 'simpandesa']);
Route::get('/desa/edit/{id}',[OnetomanyController::class, 'editdesa']);
Route::post('/desa/update',[OnetomanyController::class, 'updatedesa']);
Route::get('/desa/hapus/{id}',[OnetomanyController::class, 'hapusdesa']);

//kecamatan
Route::get('/kecamatan',[OnetomanyController::class, 'kecamatan']);
Route::get('/kecamatan/tambah',[OnetomanyController::class, 'tambahkecamatan']);
Route::post('/kecamatan/simpan',[OnetomanyController::class, 'simpankecamatan']);
Route::get('/kecamatan/edit/{id}',[OnetomanyController::class, 'editkecamatan']);
Route::post('/kecamatan/update',[OnetomanyController::class, 'updatekecamatan']);
Route::get('/kecamatan/hapus/{id}',[OnetomanyController::class, 'hapuskecamatan']);

//kota
Route::get('/kota',[OnetomanyController::class, 'kota']);
Route::get('/kota/tambah',[OnetomanyController::class, 'tambahkota']);
Route::post('/kota/simpan',[OnetomanyController::class, 'simpankota']);
Route::get('/kota/edit/{id}',[OnetomanyController::class, 'editkota']);
Route::post('/kota/update',[OnetomanyController::class, 'updatekota']);
Route::get('/kota/hapus/{id}',[OnetomanyController::class, 'hapuskota']);

//provinsi
Route::get('/provinsi',[OnetomanyController::class, 'provinsi']);
Route::get('/provinsi/tambah',[OnetomanyController::class, 'tambahprovinsi']);
Route::post('/provinsi/simpan',[OnetomanyController::class, 'simpanprovinsi']);
Route::get('/provinsi/edit/{id}',[OnetomanyController::class, 'editprovinsi']);
Route::post('/provinsi/update',[OnetomanyController::class, 'updateprovinsi']);
Route::get('/provinsi/hapus/{id}',[OnetomanyController::class, 'hapusprovinsi']);

//many to many
Route::get('/film',[ManytomanyController::class, 'film']);
Route::get('/film/tambah',[ManytomanyController::class, 'tambahfilm']);
Route::post('/film/simpan',[ManytomanyController::class, 'simpanfilm']);
Route::get('/film/edit/{id}',[ManytomanyController::class, 'editfilm']);
Route::post('/film/update',[ManytomanyController::class, 'updatefilm']);
Route::get('/film/hapus/{id}',[ManytomanyController::class, 'hapusfilm']);

Route::get('/penonton',[ManytomanyController::class, 'penonton']);
Route::get('/penonton/tambah',[ManytomanyController::class, 'tambahpenonton']);
Route::post('/penonton/simpan',[ManytomanyController::class, 'simpanpenonton']);
Route::get('/penonton/edit/{id}',[ManytomanyController::class, 'editpenonton']);
Route::post('/penonton/update',[ManytomanyController::class, 'updatepenonton']);
Route::get('/penonton/hapus/{id}',[ManytomanyController::class, 'hapuspenonton']);

Route::get('/penonton/reservasi/{id}',[ManytomanyController::class, 'reservasi']);
Route::get('/penonton/reservasi/tambah/{film_id}/{penonton_id}',[ManytomanyController::class, 'tambahreservasi']);
Route::get('/penonton/reservasi/hapus/{film_id}/{penonton_id}',[ManytomanyController::class, 'hapusreservasi']);
Route::get('/film/reservasi/hapus/{film_id}/{penonton_id}',[ManytomanyController::class, 'hapusreservasifilm']);
