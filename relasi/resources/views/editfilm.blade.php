<!doctype html>
<html lang="en">
<head>
    <title>Edit Film</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Edit Film</h2>
        <form method="post" action="/film/update">

            {{ csrf_field() }}

            <div class="form-group">
                <input type="hidden" name="id" value="{{ $data[0]->id }}">
                <label>Nama</label>
                <input type="text" name="judul" class="form-control" required value="{{$data[0]->judul}}">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
