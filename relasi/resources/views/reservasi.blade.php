<!doctype html>
<html lang="en">
<head>
    <title>List Film</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">{{$data->nama}}</h2>
        <h2 class="card-title mt-2 mb-3">List Film</h2>
    </div>
    <div class="card p-3 mt-3">
        <h1 class="card-title mt-2 mb-3">Belum Reservasi</h1>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="judul">Judul</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($belum as $b)
                <tr>
                    <td>{{ $b->id }}</td>
                    <td>{{ $b->judul }}</td>
                    <td>
                        <a href="/penonton/reservasi/tambah/{{ $b->id }}/{{ $data->id }}" class="btn btn-success">Reservasi</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="card p-3 mt-3">
        <h2 class="card-title mt-2 mb-3">Sudah Reservasi</h2>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="judul">Judul</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sudah as $s)
                <tr>
                    <td>{{ $s->id }}</td>
                    <td>{{ $s->judul }}</td>
                    <td>
                        <a href="/penonton/reservasi/hapus/{{ $s->id }}/{{ $data->id }}" class="btn btn-danger">Batal Reservasi</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
