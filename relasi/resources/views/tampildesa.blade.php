<!doctype html>
<html lang="en">
<head>
    <title>List Desa</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">List Desa</h2>
        <div class="row p-3">
            <a href="/desa" class="btn btn-primary col-2 mr-2">Lihat list desa</a>
            <a href="/kecamatan" class="btn btn-primary col-2 mr-2">Lihat list kecamatan</a>
            <a href="/kota" class="btn btn-primary col-2 mr-2">Lihat list kota</a>
            <a href="/provinsi" class="btn btn-primary col-2">Lihat list provinsi</a>
        </div>
        <a href="/desa/tambah" class="btn btn-primary col-2">Input Desa Baru</a>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="desa">Desa</th>
                <th id="kecamatan">Kecamatan</th>
                <th id="kota">Kota/Kabupaten</th>
                <th id="provinsi">Provinsi</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $d)
                <tr>
                    <td>{{ $d->id }}</td>
                    <td>{{ $d->nama }}</td>
                    <td>{{ $d->kecamatan->nama }}</td>
                    <td>{{ $d->kecamatan->kota_kabupaten->nama }}</td>
                    <td>{{ $d->kecamatan->kota_kabupaten->provinsi->nama }}</td>
                    <td>
                        <a href="/desa/edit/{{ $d->id }}" class="btn btn-primary">Edit</a>
                        <a href="/desa/hapus/{{ $d->id }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
