<!doctype html>
<html lang="en">
<head>
    <title>List Film</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">List Film</h2>
        <div class="row p-3">
            <a href="/film" class="btn btn-primary col-2 mr-2">Lihat list film</a>
            <a href="/penonton" class="btn btn-primary col-2 mr-2">Lihat list penonton</a>
        </div>
        <a href="/film/tambah" class="btn btn-primary col-2">Input Film Baru</a>
    </div>
    @foreach($data as $d)
    <div class="card p-3">
        <div class="row p-3">
            <h3 class="card-title col-9">{{$d->id}}. {{$d->judul}}</h3>
            <a href="/film/edit/{{ $d->id }}" class="btn btn-primary">Edit</a>
            <a href="/film/hapus/{{ $d->id }}" class="btn btn-danger ml-1">Hapus</a>
        </div>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="penonton">Penonton</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($d->penonton as $p)
                <tr>
                    <td>{{ $p->id }}</td>
                    <td>{{ $p->nama }}</td>
                    <td>
                        <a href="/film/reservasi/hapus/{{$d->id}}/{{ $p->id }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @endforeach
</div>
</body>
