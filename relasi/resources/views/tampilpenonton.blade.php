<!doctype html>
<html lang="en">
<head>
    <title>List Penonton</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">List Penonton</h2>
        <div class="row p-3">
            <a href="/film" class="btn btn-primary col-2 mr-2">Lihat list film</a>
            <a href="/penonton" class="btn btn-primary col-2 mr-2">Lihat list penonton</a>
        </div>
        <a href="/penonton/tambah" class="btn btn-primary col-2">Input Penonton Baru</a>
    </div>
    <div class="card p-3">
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="penonton">Penonton</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $d)
                <tr>
                    <td>{{ $d->id }}</td>
                    <td>{{ $d->nama }}</td>
                    <td>
                        <a href="/penonton/reservasi/{{ $d->id }}" class="btn btn-success">Reservasi</a>
                        <a href="/penonton/edit/{{ $d->id }}" class="btn btn-primary">Edit</a>
                        <a href="/penonton/hapus/{{ $d->id }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
