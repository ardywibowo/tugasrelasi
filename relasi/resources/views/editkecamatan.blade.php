<!doctype html>
<html lang="en">
<head>
    <title>Edit Kecamatan</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Edit Kecamatan</h2>
        <form method="post" action="/kecamatan/update">

            {{ csrf_field() }}

            <div class="form-group">
                <input type="hidden" name="id" value="{{ $data->id }}">
                <label>Nama</label>
                <input type="text" name="nama" class="form-control" required value="{{$data->nama}}">
            </div>
            <div class="form-group">
                <label>Kota/Kabupaten</label></br>
                <select class="form-select" aria-label="Default select example" name="kota_id" required>
                    <option selected value="{{$data->kota_kabupaten->id}}">{{$data->kota_kabupaten->nama}}</option>
                    @foreach($kota as $d)
                        <option value="{{$d->id}}">{{$d->nama}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
