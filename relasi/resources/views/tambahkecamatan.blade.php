<!doctype html>
<html lang="en">
<head>
    <title>Tambah Kecamatan</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Tambah Kecamatan</h2>
        <form method="post" action="/kecamatan/simpan">

            {{ csrf_field() }}

            <div class="form-group">
                <label class="form-label">Nama</label>
                <input type="text" name="nama" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="form-label">Kota/Kabupaten</label></br>
                <select class="form-select" aria-label="Default select example" name="kota_id" required>
                    <option selected value="">Pilih Kota/Kabupaten</option>
                    @foreach($data as $d)
                        <option value="{{$d->id}}">{{$d->nama}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
