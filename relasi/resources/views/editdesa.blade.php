<!doctype html>
<html lang="en">
<head>
    <title>Edit Desa</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Edit Desa</h2>
        <form method="post" action="/desa/update">

            {{ csrf_field() }}

            <div class="form-group">
                <input type="hidden" name="id" value="{{ $data->id }}">
                <label>Nama</label>
                <input type="text" name="nama" class="form-control" required value="{{$data->nama}}">
            </div>
            <div class="form-group">
                <label>Kecamatan</label></br>
                <select class="form-select" aria-label="Default select example" name="kecamatan_id" required>
                    <option selected value="{{$data->kecamatan->id}}">{{$data->kecamatan->nama}}</option>
                    @foreach($kecamatan as $d)
                        <option value="{{$d->id}}">{{$d->nama}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
