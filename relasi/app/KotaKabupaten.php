<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KotaKabupaten extends Model
{
    protected $table = 'kota_kabupaten';

    public function provinsi()
    {
        return $this->belongsTo('App\Provinsi');
    }

    public function kecamatan()
    {
        return $this->hasMany('App\Kecamatan');
    }
}
