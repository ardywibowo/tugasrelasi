<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penonton extends Model
{
    protected $table = 'penonton';

    public function film()
    {
        return $this->belongsToMany('App\Penonton');
    }
}
