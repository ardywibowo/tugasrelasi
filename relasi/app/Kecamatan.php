<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'kecamatan';

    public function kota_kabupaten()
    {
        return $this->belongsTo('App\KotaKabupaten');
    }

    public function desa()
    {
        return $this->hasMany('App\Desa');
    }
}
