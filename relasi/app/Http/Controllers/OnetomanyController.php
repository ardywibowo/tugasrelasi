<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Desa;
use App\KotaKabupaten;
use App\Kecamatan;
use App\Provinsi;
use Illuminate\Support\Facades\DB;

class OnetomanyController extends Controller
{
    //desa
    public function desa()
    {
        $data = Desa::all();
        return view('tampildesa', ['data'=>$data]);
    }

    public function tambahdesa()
    {
        $data = Kecamatan::all();
        return view('tambahdesa', ['data'=>$data]);
    }

    public function simpandesa(Request $request)
    {
        DB::table('desa')->insert([
            'nama' => $request->nama,
            'kecamatan_id' => $request->kecamatan_id
        ]);

        return redirect('/desa');
    }

    public function editdesa($id)
    {
        $data = Desa::find($id);
        $kecamatan = Kecamatan::all();
        return view('editdesa', ['data'=>$data, 'kecamatan'=>$kecamatan]);
    }

    public function updatedesa(Request $request)
    {
        DB::table('desa')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'kecamatan_id' => $request->kecamatan_id
        ]);

        return redirect('/desa');
    }

    public function hapusdesa($id)
    {
        DB::table('desa')->where('id', $id)->delete();

        return redirect('/desa');
    }

    //kecamatan
    public function kecamatan()
    {
        $data = Kecamatan::all();
        return view('tampilkecamatan', ['data'=>$data]);
    }

    public function tambahkecamatan()
    {
        $data = KotaKabupaten::all();
        return view('tambahkecamatan', ['data'=>$data]);
    }

    public function simpankecamatan(Request $request)
    {
        DB::table('kecamatan')->insert([
            'nama' => $request->nama,
            'kota_kabupaten_id' => $request->kota_id
        ]);

        return redirect('/kecamatan');
    }

    public function editkecamatan($id)
    {
        $data = Kecamatan::find($id);
        $kota = KotaKabupaten::all();
        return view('editkecamatan', ['data'=>$data, 'kota'=>$kota]);
    }

    public function updatekecamatan(Request $request)
    {
        DB::table('kecamatan')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'kota_kabupaten_id' => $request->kota_id
        ]);

        return redirect('/kecamatan');
    }

    public function hapuskecamatan($id)
    {
        DB::table('desa')->where('kecamatan_id', $id)->delete();
        DB::table('kecamatan')->where('id', $id)->delete();

        return redirect('/kecamatan');
    }


    //kota
    public function kota()
    {
        $data = KotaKabupaten::all();
        return view('tampilkota', ['data'=>$data]);
    }

    public function tambahkota()
    {
        $data = Provinsi::all();
        return view('tambahkota', ['data'=>$data]);
    }

    public function simpankota(Request $request)
    {
        DB::table('kota_kabupaten')->insert([
            'nama' => $request->nama,
            'provinsi_id' => $request->provinsi_id
        ]);

        return redirect('/kota');
    }

    public function editkota($id)
    {
        $data = KotaKabupaten::find($id);
        $provinsi = Provinsi::all();
        return view('editkota', ['data'=>$data, 'provinsi'=>$provinsi]);
    }

    public function updatekota(Request $request)
    {
        DB::table('kota_kabupaten')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'provinsi_id' => $request->provinsi_id
        ]);

        return redirect('/kota');
    }

    public function hapuskota($id)
    {
        DB::delete('delete from desa where kecamatan_id = any (select id from kecamatan where kota_kabupaten_id = :id)', ['id' => $id]);
        DB::table('kecamatan')->where('kota_kabupaten_id', $id)->delete();
        DB::table('kota_kabupaten')->where('id', $id)->delete();

        return redirect('/kota');
    }


    //provinsi
    public function provinsi()
    {
        $data = Provinsi::all();
        return view('tampilprovinsi', ['data'=>$data]);
    }

    public function tambahprovinsi()
    {
        return view('tambahprovinsi');
    }

    public function simpanprovinsi(Request $request)
    {
        DB::table('provinsi')->insert([
            'nama' => $request->nama
        ]);

        return redirect('/provinsi');
    }

    public function editprovinsi($id)
    {
        $data = Provinsi::find($id);
        return view('editprovinsi', ['data'=>$data]);
    }

    public function updateprovinsi(Request $request)
    {
        DB::table('provinsi')->where('id', $request->id)->update([
            'nama' => $request->nama
        ]);

        return redirect('/provinsi');
    }

    public function hapusprovinsi($id)
    {
        DB::delete('delete from desa where kecamatan_id = any (select id from kecamatan where kota_kabupaten_id = any (select id from kota_kabupaten where provinsi_id = :id))', ['id' => $id]);
        DB::delete('delete from kecamatan where kota_kabupaten_id = any (select id from kota_kabupaten where provinsi_id = :id)', ['id' => $id]);
        DB::table('kota_kabupaten')->where('provinsi_id', $id)->delete();
        DB::table('provinsi')->where('id', $id)->delete();

        return redirect('/provinsi');
    }
}
