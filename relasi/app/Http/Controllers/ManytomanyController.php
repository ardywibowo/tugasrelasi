<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penonton;
use App\Film;
use Illuminate\Support\Facades\DB;

class ManytomanyController extends Controller
{
    //film
    public function film()
    {
        $data = Film::all();
        return view('tampilfilm', ['data'=>$data]);
    }

    public function tambahfilm()
    {
        return view('tambahfilm');
    }

    public function simpanfilm(Request $request)
    {
        DB::table('film')->insert([
            'judul' => $request->judul
        ]);

        return redirect('/film');
    }

    public function editfilm($id)
    {
        $data = DB::table('film')->where('id', $id)->get();
        return view('editfilm', ['data'=>$data]);
    }

    public function updatefilm(Request $request)
    {
        DB::table('film')->where('id', $request->id)->update([
            'judul' => $request->judul
        ]);

        return redirect('/film');
    }

    public function hapusfilm($id)
    {
        DB::table('film_penonton')->where('film_id', $id)->delete();
        DB::table('film')->where('id', $id)->delete();

        return redirect('/film');
    }

    //penonton
    public function penonton()
    {
        $data = Penonton::all();
        return view('tampilpenonton', ['data'=>$data]);
    }

    public function tambahpenonton()
    {
        return view('tambahpenonton');
    }

    public function simpanpenonton(Request $request)
    {
        DB::table('penonton')->insert([
            'nama' => $request->nama
        ]);

        return redirect('/penonton');
    }

    public function editpenonton($id)
    {
        $data = DB::table('penonton')->where('id', $id)->get();
        return view('editpenonton', ['data'=>$data]);
    }

    public function updatepenonton(Request $request)
    {
        DB::table('penonton')->where('id', $request->id)->update([
            'nama' => $request->nama
        ]);

        return redirect('/penonton');
    }

    public function hapuspenonton($id)
    {
        DB::table('film_penonton')->where('penonton_id', $id)->delete();
        DB::table('penonton')->where('id', $id)->delete();

        return redirect('/penonton');
    }

    public function reservasi($id)
    {
        $data = Penonton::find($id);
        $sudah = DB::select('select * from film where id = any (select film_id from film_penonton where penonton_id = :id)', ['id' => $id]);
        $belum = DB::select('select * from film where id not in (select film_id from film_penonton where penonton_id = :id)', ['id' => $id]);
        return view('reservasi', ['data'=>$data, 'sudah'=>$sudah, 'belum'=>$belum]);
    }

    public function tambahreservasi($film_id,$penonton_id)
    {
        DB::table('film_penonton')->insert([
            'film_id' => $film_id,
            'penonton_id' => $penonton_id
        ]);

        return redirect('/penonton/reservasi/'.$penonton_id);
    }

    public function hapusreservasi($film_id,$penonton_id)
    {
        DB::table('film_penonton')->where([
            ['film_id', $film_id],
            ['penonton_id', $penonton_id],
        ])->delete();

        return redirect('/penonton/reservasi/'.$penonton_id);
    }

    public function hapusreservasifilm($film_id,$penonton_id)
    {
        DB::table('film_penonton')->where([
            ['film_id', $film_id],
            ['penonton_id', $penonton_id],
        ])->delete();

        return redirect('/film');
    }
}
